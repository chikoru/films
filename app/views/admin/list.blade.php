@extends('layouts.main')

@section('title')
	<title>User List</title>
@stop

@section('css')
	<link rel="stylesheet" href="{{ URL::asset('css/site.css') }}"> 
@stop

@section('content')

	<h2>User list</h2>
	
	<table class="table">
			<thead>
				<tr>
					<th>Email</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($users as $user) : ?>	
				<tr>
					<td>{{$user->email}}</td>
					<td>{{$user->first_name}}</td>
					<td>{{$user->last_name}}</td>
				</tr>
			<?php  endforeach; ?>
			</tbody>
		</table>

	<nav>
	  <ul class="pagination">
	    <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
	    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
	    <li><a href="#">2</a></li>
	    <li><a href="#">3</a></li>
	    <li><a href="#">4</a></li>
	    <li><a href="#">5</a></li>
	    <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
	  </ul>
	</nav> 
	
@stop