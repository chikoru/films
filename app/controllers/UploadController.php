 <?php

class UploadController extends BaseController {

	public function upload_file(){
		
		$allowed = ['png', 'jpg', 'jpeg'];
		$upload_dir = 'uploads';
		$upload_thumbs_dir = 'uploads/thumbs';
		$uploaded = [];

		if(!empty($_FILES['files'])){

			foreach ($_FILES['files']['name'] as $key => $name) {

				if($_FILES['files']['error'][$key] === 0){

					$temp = $_FILES['files']['tmp_name'][$key];

					$ext = explode('.', $name);
					$ext = strtolower(end($ext));

					$file = md5($temp).time().'.'.$ext;

					$size = $_FILES['files']['size'][$key];


					if(in_array($ext, $allowed)){

						if($size <= 2048000){

							$images_size = getimagesize($temp);
							$image_width = $images_size[0];
							$image_height = $images_size[1];
							
							$new_width = 100;
							$new_height= ($image_height/$image_width) * $new_width;

							$thumb = $new_width.'_'.$file;
							
							$new_image = imagecreatetruecolor($new_width, $new_height);

							if($ext == 'jpg' || $ext == 'jpeg' ){
								$old_image = imagecreatefromjpeg($temp);
							}elseif($ext == 'png'){
								$old_image = imagecreatefrompng($temp);
							}
							
							imagecopyresampled($new_image, $old_image, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height);
							
							if(file_exists($upload_dir) == false){
								mkdir($upload_dir);

								if(file_exists($upload_thumbs_dir) == false){
									mkdir($upload_thumbs_dir);
								}
							}

							if(imagejpeg($old_image, $upload_dir.'/'.$file, 100) && imagejpeg($new_image, $upload_thumbs_dir.'/'.$thumb, 100)){
								
								$file_upload = array(
									'name' => $file, 
									'thumb' => $thumb
								);
								array_push($uploaded, $file_upload);
							}
						}
						
					}
					
				}
			}
		}	
		if(!empty($uploaded)){
			return Response::json(array(
				'files' => $uploaded
			));
		}
	}

	public function delete_file(){

		if(isset($_POST['fname'])){

			$upload = 'uploads/'.$_POST['fname'];
			$upload_thumbs = 'uploads/thumbs/100_'.$_POST['fname'];

			if(unlink($upload)){

				if(unlink($upload_thumbs)){
					return Response::json(array(
						'success' => true
					));
				}
			}

		}
	}
}

