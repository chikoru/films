@extends('layouts.main')

@section('title')
	<title>Change Password</title>
@stop

@section('css')
	<link rel="stylesheet" href="{{ URL::asset('css/site.css') }}">
@stop

@section('content')

	
	<form id="change-password" action="{{ URL::route('post-change-password') }}" method="post">
		<span class="alert"></span>
	  	<div class="form-group">
	    	<label for="old_password">Old Password</label>
	    	<input type="text" class="form-control" id="old_password" name="old_password">
	  	</div>
		<div class="form-group">
	    	<label for="new_password">New Password</label>
	    	<input type="text" class="form-control" id="new_password" name="new_password">
	  	</div>
	  	<div class="form-group">
	    	<label for="password_again">Password Again</label>
	    	<input type="text" class="form-control" id="password_again" name="password_again">
	  	</div>

		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">  		  	
	  	<button type="submit" class="btn btn-primary">Change</button>
	</form>
@stop

@section('script')
	<script src="{{ URL::asset('js/change-password.js') }}"></script>
@stop