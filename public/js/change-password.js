(function ($){

  $('form#change-password').on('submit', function(){

  	var form = $(this),
  		url = form.attr('action'),
  		method = form.attr('method');

       //flag    
/*    	var flag = true;	

      // Check null

      if (form.find('#old_password').val() == '') {
          flag = false;
      }
      if (form.find('#new_password').val() == '') {
          flag = false;
      }
      if (form.find('#password_again').val() == '') {
          flag = false;
      }

      if(!flag){
        form.find('.alert').showMessageHTML('Lỗi!','Bạn phải nhập đầy đủ thông tin.','danger');
        return false;
      }
    
      // Check password 
      if (form.find('#old_password').val().length <= 6 || form.find('#old_password').val().length > 20) {
        form.find('#old_password').parent().addError();
        form.find('.alert').showMessageHTML('Lỗi!','Mật khẩu phải từ 6-20 kí tự.','danger');
        return false;
      }

      if (form.find('#new_password').val().length <= 6 || form.find('#new_password').val().length > 20) {
        form.find('#new_password').parent().addError();
        form.find('.alert').showMessageHTML('Lỗi!','Mật khẩu phải từ 6-20 kí tự.','danger');
        return false;
      }

      if (form.find('#password_again').val().length <= 6 || form.find('#password_again').val().length > 20) {
        form.find('#password_again').parent().addError();
        form.find('.alert').showMessageHTML('Lỗi!','Mật khẩu phải từ 6-20 kí tự.','danger');
        return false;
      }

      if (form.find('#old_password').val() == form.find('#new_password').val()) {
        form.find('#new_password').parent().addError();
        form.find('.alert').showMessageHTML('Lỗi!','Mật khẩu mới không được giống mật khẩu cũ.','danger');
        return false;
      }

      if (form.find('#password_again').val() != form.find('#new_password').val()) {
        form.find('#password_again').parent().addError();
        form.find('.alert').showMessageHTML('Lỗi!','Mật khẩu không trùng nhau.','danger');
        return false;
      }*/

  	$.ajax({
  	  	type: 'post',
  	  	url: url,
  	  	data: form.serialize(),
  	   	dataType: 'json',
  	  	success: function(res){
            console.log(res);
    	  		/*if(res.success == true){
    	  			form.find('.alert').showMessageHTML('Thành công!','Bạn đã thay đổi mật khẩu thành công.'+ res.link ,'success');
    	  		} else {
              form.find('.alert').showMessageHTML('Thông báo!','Mật khẩu cũ không đúng.','info');
            }*/
  	  	}
  	  	/*error: function(){
           form.find('.alert').showMessageHTML('Cảnh báo!','Có lỗi xảy ra.','warning');
  	  	}*/
  	});

  	return false;

  });

})(jQuery)