(function ($){

  $('form#update-profile').on('submit', function(){

  	var form = $(this),
  		url = form.attr('action'),
  		method = form.attr('method');

    	//regex
    	var name_regex = /^[a-zA-Z ]+$/,
    	    email_regex = /^[\w\-\.\_\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

       //flag    
    	var flag = true;	


      // Check null

      if (form.find('#first_name').val() == '') {
          flag = false;
      }
      if (form.find('#last_name').val() == '') {
          flag = false;
      }
      if (form.find('#email').val() == '') {
          flag = false;
      }

      if(!flag){
        form.find('.alert').showMessageHTML('Lỗi!','Bạn phải nhập đầy đủ thông tin.','danger');
        return false;
      }
      
      // Check email
      if (!form.find('#email').val().match(email_regex)) {
        form.find('#email').parent().addError();
        form.find('.alert').showMessageHTML('Lỗi!','Vui lòng nhập đúng email.','danger');
        return false;
      }

      // Check fname - lname
      if(!form.find('#first_name').val().match(name_regex)){
        form.find('#first_name').parent().addError();
        form.find('.alert').showMessageHTML('Lỗi!','Vui lòng nhập tên không dấu.','danger');
        return false;
      }

      if(!form.find('#last_name').val().match(name_regex)){
        form.find('#last_name').parent().addError();
        form.find('.alert').showMessageHTML('Lỗi!','Vui lòng nhập tên không dấu.','danger');
        return false;
      }


  	$.ajax({
  	  	type: 'post',
  	  	url: url,
  	  	data: form.serialize(),
  	   	dataType: 'json',
  	  	success: function(res){
  	  		if(res.success == true){
  	  			form.find('.alert').showMessageHTML('Thành công!','Thông tin của bạn đã được thay đổi.','success');
  	  		} else {
            form.find('.alert').showMessageHTML('Thất bại!','Không thể cập nhật!.','warning');
          }
  	  	},
  	  	error: function(){
          form.find('.alert').showMessageHTML('Lỗi!','Có lỗi xảy ra.','warning');
  	  	}
  	});

  	return false;

  });


})(jQuery)