<?php

class AdminController extends BaseController {

	public function viewAdminPage(){
		return View::make('admin.home');
	}

	public function getCreate(){
		return View::make('admin.create');
	}

	public function postCreate(){
		$validator = Validator::make(Input::all(), array(
			'email' => 'required|max:50|email|unique:users',
			'password' => 'required|min:6',
			'password_again' =>'required|same:password'
		));

		if(!$validator->fails()){

			$user = User::create(array(
				'email' => Input::get('email'),
				'first_name' => Input::get('first_name'),
				'last_name' => Input::get('last_name'),
				'password' => Hash::make(Input::get('password')),
				'photo' => Input::get('file_upload')
			));

			if($user){
				$user->save();
				return Response::json(array('success' => true));
			}
		}
		return Response::json(array('success' => false));
	}

	public function getUsersList(){
		$users = User::paginate(5);
		return View::make('admin.users-list')->with(array(
				'users' => $users,
				'row_number' => 5
			));
	}

	public function deleteUser($id){
		$user = User::where('id', '=', $id)->first();
		if ($user) {
			$user->delete();
		}	
	}

}
