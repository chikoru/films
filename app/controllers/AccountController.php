<?php

class AccountController extends BaseController {


	public function getCreate(){
		return View::make('account.register');
	}	

	public function postCreate(){


		// Check header content type
		if (Request::ajax())
		{
		    // Validate params
			$validator = Validator::make(Input::all(), array(
				'email' => 'required|max:50|email|unique:users',
				'password' => 'required|min:6',
				'password_again' =>'required|same:password'
			));

			if(!$validator->fails()){

				// Create User
				$user = User::create(array(
					'email' => Input::get('email'),
					'first_name' => Input::get('first_name'),
					'last_name' => Input::get('last_name'),
					'password' => Hash::make(Input::get('password')),
					'photo' => Input::get('file_upload')
				));


				if($user){

					$code = str_random(60);
					$user->code = $code;

					if ($user->save()) {
						Mail::send('emails.auth.activate', array('link' => URL::route('activate-account', $code), 'username' => $user->first_name), function($message) use ($user){
							$message->to($user->email, $user->first_name)->subject('Confirm registration !');
						});
						return Response::json(array('success' => true));
					}
				}
			}
		} 		
	}

	public function activate($code){

		$code_regex = "/^[a-zA-Z0-9]+$/";

		if (preg_match($code_regex, $code)) {

			$user = User::where('code', '=', $code)->first();

			$user->code = '';
			if ($user->active == 0) {
				$user->active = 1;
			}
			$user->save();

			return View::make('emails.auth.login');
		}		
	}

	public function getLogin(){
		return View::make('emails.auth.login');
	}

	public function postLogin(){
		$validator = Validator::make(Input::all(),
			array(
				'email' => 'required|email',
				'password' => 'required'
		));

		if($validator->fails()){
			return Redirect::route('get-login')
				->withErrors($validator)
				->withInput();	
		}else{

			//Sign In 
			$email = Input::get('email');
			$password = Input::get('password');


			$remember = (Input::has('remember')) ? true : false;

			$auth = Auth::attempt(array(
				'email' => $email,
				'password' => $password
			), $remember);

			if($auth){

				if(Auth::user()->is_admin == 1 && Auth::user()->active == 1){
					return Redirect::route('administrator-account-users');
				}elseif (Auth::user()->active == 0) {
					Auth::logout();
					return Redirect::route('get-login')
						->with('global', 'Tài khoản của bạn chưa được kích hoạt , vui lòng kiểm tra lại email !');
				}
				elseif (Auth::user()->active == 2) {
					Auth::logout();
					return Redirect::route('get-login')
						->with('global', 'Tài khoản của bạn đã bị khoá , vui lòng liên hệ <strong>admin@minhnguyen.vn</strong> để biết thêm chi tiết');
				}

				return Redirect::route('home');			
			}else{

				return Redirect::route('get-login')
					->with('global', '<strong>Tên đăng nhập</strong> hoặc <strong>Mật khẩu</strong> không chính xác');
			}

			return Redirect::route('get-login')
				->with('global', 'There are problem signing you in.');
		}
	}

	public function signout(){
		Auth::logout();
		return View::make('emails.auth.login');
	}

	public function getSendEmail(){
		return View::make('emails.auth.link');
	}

	public function postForgotPassword(){

		if (Request::ajax()) {

			$validator = Validator::make(Input::all(),
				array(
					'email' => 'required|email'
				)	
			);

			if($validator->fails()){
				return Redirect::route('account-forgot-password')
					->withErrors($validator)
					->withInput();
			}else{
				
				$user = User::where('email', '=', Input::get('email'));

				if($user->count()){
					$user = $user->first();

					//Generate new code and password
					$code = str_random(60);
					$password = str_random(10);

					$user->code = $code;
					$user->password_tmp = Hash::make($password);

					if($user->save()){

						Mail::send('emails.auth.forgot', array('link' => URL::route('account-recover', $code), 'username' => $user->first_name, 'password' => $password), function($message) use ($user){
							$message->to($user->email, $user->first_name)->subject('Mật khẩu mới !');
						});

						return Response::json(array('success' => true));	
					}
				}
				return Response::json(array('success' => false));
			}

		}					
	}

	public function getRecover($code){
		
		$user = User::where('code', '=', $code)
				->where('password_tmp', '!=', '');

		if($user->count()){
			$user = $user->first();

			$user->password = $user->password_tmp;
			$user->password_tmp = '';
			$user->code = '';


			if($user->save()){
				return Redirect::route('home')
					->with('global', 'Your account has been recover !');		 
			}
		}
		return Redirect::route('home')
			->with('global', 'Could not recover your account !');
	}

	public function viewProfile(){
		return View::make('account.profile');
	}

	public function updateProfile(){

		$validator = Validator::make(Input::all(), array(
			'first_name' => 'required',
			'last_name' =>'required'
		));

		if(!$validator->fails()){

			$difference = false;

			$user = Auth::user();

			if ($user->first_name != Input::get('first_name'))
			{
				$user->first_name = Input::get('first_name');
				$difference = true;
			}
			if ($user->last_name != Input::get('last_name'))
			{
				$user->last_name = Input::get('last_name');
				$difference = true;
			}
			if ($user->photo != Input::get('file_upload'))
			{
				$user->photo = Input::get('file_upload');
				$difference = true;
			}

			if (!$difference) {
				return Response::json(array('success' => false));
			} else {
				$user->save();
				return Response::json(array('success' => true));
			}	
		} 
	}

	public function getChangePassword(){
		return View::make('account.change-password');
	}

	public function postChangePassword(){

		if (Request::ajax()) {
			
			// Validate params
			$validator = Validator::make(Input::all(), 
				array(
					'old_password' => 'required|min:6',
					'new_password' => 'required|min:6',
					'password_again' =>'required|same:new_password'
				)
			);
			

			if(!$validator->fails()){
				
				// Create User
				if (Auth::check()) {

					$user = Auth::user();

					/*if ($user->password == Hask::make(Input::get('old_password'))) {

						$user->password = Hask::make(Input::get('new_password'));
						$user->save();

						return Response::json(array('success' => true));

					} else {
						return Response::json(array('success' => false));
					}*/

					return Response::json(array('success' => true, 'link' => $user->id));
				}
				return Response::json(array('success' => false));
			}

			
		}
	}

}

