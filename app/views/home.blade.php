@extends('layouts.main')

@section('title')
	<title>Home</title>
@stop

@section('content')

	<h1>HOME</h1>

	@if(Auth::check())	
		Xin chào <strong> {{ Auth::user()->first_name }} </strong><br/>
		<img src="{{ URL::asset('uploads/thumbs/100_'.Auth::user()->photo) }}" alt="Avatar"><br/><br/>
		<button type="submit" class="btn btn-primary">
			<a style="color:white" href="{{ URL::route('account-view-profile') }}">View Profile</a>
		</button>
		<button type="submit" class="btn btn-primary">
			<a style="color:white" href="{{ URL::route('get-change-password') }}">Change Password</a>
		</button>
		<button type="submit" class="btn btn-primary">
			<a style="color:white" href="{{ URL::route('account-sign-out') }}">Sign Out</a>
		</button>
		
	@else
		<button type="submit" class="btn btn-primary">
			<a style="color:white" href="{{ URL::route('get-login') }}">Login</a>
		</button>	
	@endif

	@if(Session::has('global'))
		<div class="alert alert-info">{{ Session::get('global') }}</div>
	@endif
		
		
@stop 

@section('script')

@stop



