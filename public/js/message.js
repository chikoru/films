 //Show message
  $.fn.showMessageHTML = function(title, message, type){
    var self = this, typeClass;

    self.find('.alert').remove();

    switch(type){

      case 'success':

        typeClass = 'alert-' + type;
        message   = '<strong>'+ title +'</strong> ' + message;

        break;

      case 'info':

        typeClass = 'alert-' + type;
        message   = '<strong>'+ title +'</strong> ' + message;

        break;
      case 'warning':

        typeClass = 'alert-' + type;
        message   = '<strong>'+ title +'</strong> ' + message;

        break;
      case 'danger':

        typeClass = 'alert-' + type;
        message   = '<strong>'+ title +'</strong> ' + message;

        break;
    }

    if(message != ''){

      self.prepend('<div class="alert ' + typeClass + '" style="display: none" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');

      self.find('.alert').fadeIn('fast');

      self.find('.alert').fadeOut(3000);
    }
  };

  // Add error
  $.fn.addError = function(){
    var self = $(this);
    self.addClass('has-error has-feedback');
    self.append('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
  };

  $(document).ready(function (){

    $('.form-control').focus(function(){
      $(this).parent().removeClass('has-error has-feedback');
      $(this).parent().find('.form-control-feedback').remove();
    })

  })