<?php 

	use PhpAmqpLib\Connection\AMQPStreamConnection;
	use PhpAmqpLib\Message\AMQPMessage;
	use PhpAmqpLib\Connection\AMQPConnection;

	use Illuminate\Support\Facades\Input;
	use Illuminate\Contracts\Routing\ResponseFactory;

	class ProducerController extends BaseController{

		private $connection;
		private $channel;

		public function __construct(){

			$this->connection = new AMQPConnection('localhost', 5672, 'root', 'root');
			$this->channel = $this->connection->channel();
		}

		public function taskQueues($queue_name){

			if(Input::has('message')){

				$message = Input::get('message');

				$this->channel->queue_declare($queue_name, false, false, false, false);

				$msg = new AMQPMessage($message);

				$this->channel->basic_publish($msg, '', $queue_name);

				$this->close();

				return Response::json(array('status' => 'success', 'data' => $message));
			}
		}

		public function pubSub(){

			if(Input::has('message')){

				$message = Input::get('message');

				$this->channel->exchange_declare('logs', 'fanout', false, false, false);

				$msg = new AMQPMessage($message);

				$this->channel->basic_publish($msg, 'logs');

				$this->close();

			  	/*return response()->json([
                	'status' => 'success',
                	'data' => 'Sent: '.$message
        	  	]);*/
        	  	return Response::json(array('status' => 'success', 'data' => $message));
			}
			
		}

		private function close(){	
			$this->channel->close();
			$this->connection->close();
		}

	}


?>