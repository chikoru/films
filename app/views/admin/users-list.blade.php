@extends('layouts.main')

@section('title')
	<title>User List</title>
@stop

@section('css')
	<link rel="stylesheet" href="{{ URL::asset('css/site.css') }}"> 
@stop

@section('content')

	<h2>User list</h2>
	
	<div class='list-users'>
		<table class="table">
			<thead>
				<tr>
					<th>STT</th>
					<th>Email</th>
					<th>First Name</th>
					<th>Last Name</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($users as $user) : ?>	
				<tr class="user-row-{{$user->id}}">
					<td>{{$user->id}}</td>
					<td>{{$user->email}}</td>
					<td>{{$user->first_name}}</td>
					<td>{{$user->last_name}}</td>
					<td>
						<a href="{{URL::route('administrator-delete-user', $user->id)}}" class="user-delete">Delete</a>
					</td>
					<td>
						<a href="#" class="user-block">Block</a>
					</td>	
				</tr>
			<?php  endforeach; ?>
			</tbody>
		</table>
	</div>

	<div class="pagination"> {{ $users->links() }} </div>
	
@stop

@section('script')
	<script type="text/javascript">
		$('.user-delete').on('click', function(event){
			/*event.preventDefault();*/
			var c = $(this);
			var url = c.attr('href');
			$.ajax({
				type: 'post',
		  	  	url: url,
		  	  	success: function(res){
		  	  		c.parent().parent().remove();
		  	  	},
		  	  	error: function(){
		          	alert('Something wrong !!!');
		  	  	}
			});

			return false;
		});

		$('.user-block').on('click', function(event){
			var c = $(this);
			c.parent().remove();
		});
	</script>
@stop