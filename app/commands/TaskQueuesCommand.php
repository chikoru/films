<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use PhpAmqpLib\Connection\AMQPConnection;

class TaskQueuesCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mq:work';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Distributing tasks among workers';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		if($this->argument('queue_name')){

            $queue_name = $this->argument('queue_name');

            if ($this->confirm('Do you wish to continue? [y|n]')){

                $connection = new AMQPConnection('localhost', 5672, 'root', 'root');
                $channel = $connection->channel();

                $channel->queue_declare($queue_name, false, false, false, false);

                echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";


                //-----------------------Execute Message--------------------------------------------	
                $callback = function($msg) {
                   echo " [x] Received : ", $msg->body, "\n";
                };

                $channel->basic_consume($queue_name, '', false, true, false, false, $callback);

                while(count($channel->callbacks)) {
                    $channel->wait();
                }

                $channel->close();
                $connection->close();
            }

        }else{

            $this->error('Queue name is required !');
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
            array('queue_name', InputArgument::OPTIONAL, 'Queue name argument !')
        );
	}


}
