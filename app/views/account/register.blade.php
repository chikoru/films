@extends('layouts.main')

@section('title')
	<title>Register</title>
@stop

@section('css')
	<link rel="stylesheet" href="{{ URL::asset('css/site.css') }}">
@stop

@section('content')

	
	<form id="register" action="{{ URL::route('post-create') }}" method="post">
		<span class="alert"></span>
	  	<div class="form-group">
	    	<label for="email">Email</label>
	    	<input type="text" class="form-control" id="email" name="email">
	  	</div>
		<div class="form-group">
	    	<label for="email">Password</label>
	    	<input type="text" class="form-control" id="password" name="password">
	  	</div>
	  	<div class="form-group">
	    	<label for="email">Password Again</label>
	    	<input type="text" class="form-control" id="password_again" name="password_again">
	  	</div>
	  	<div class="form-group">
	    	<label for="email">Last name</label>
	    	<input type="text" class="form-control" id="last_name" name="last_name">
	  	</div>
	  	<div class="form-group">
	    	<label for="email">First name</label>
	    	<input type="text" class="form-control" id="first_name" name="first_name">
	  	</div>
	  	<div class="file-input">
	  		<label>Select file ...</label>
	  		<div class="file-preview">
	  			<!-- <div class="close fileinput-remove">x</div> -->
	  			<div class>
	  				<div class="file-preview-thumbnails">
	  					<!-- add image here-->
	  				</div>
	  				<div class="progress" id="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
						    <span class="progressText"></span>
						</div>
					</div>
	  			</div>
	  		</div>
	  		<div class="input-group">
			  	<div tabindex="500" class="form-control file-caption  kv-fileinput-caption">
			  		<div class="file-caption-name"></div>
			  	</div>
			  	<div class="input-group-btn" >
			  		<div tabindex="500" class="btn btn-primary btn-file">
			    		<i class="glyphicon glyphicon-folder-open"></i>&nbsp;Browse …
			    		<input id="file_upload" type="file" class="file">  
			   		</div>	
			  	</div>
		  	</div>
	  	</div>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">  		  	
	  	<button type="submit" class="btn btn-primary">Register</button>
	</form>
@stop

@section('script')
	<script src="{{ URL::asset('js/upload.js') }}"></script>
	<script src="{{ URL::asset('js/register.js') }}"></script>
@stop