@extends('layouts.main')

@section('title')
	<title>My profile</title>
@stop

@section('css')
	<link rel="stylesheet" href="{{ URL::asset('css/site.css') }}"> 
	<style type="text/css">
		/* preview image*/
		.file-preview{
		    display: inline;
		}

		.form-control{
			width : 80%;
			display: inline;
		}


	</style>
@stop

@section('content')

	<h2>My profile :)</h2>
	<?php
		$email = Auth::user()->email;
		$first_name = Auth::user()->first_name;
		$last_name = Auth::user()->last_name;
		$province_id = Auth::user()->province_id;
		$district_id = Auth::user()->district_id;
		$photo = Auth::user()->photo;

	?>

	<form id="update-profile" action="{{ URL::route('account-update-profile') }}" method="post">
		<span class="alert"></span>
	  	<div class="form-group">
	    	<label for="email">Email</label>
	    	<input type="text" class="form-control" id="email" name="email" value="{{ $email }}" disabled>
	    	<!-- <button type="submit" class="btn btn-primary">
	    		<a href="#" style="color:white">Update</a>
	    	</button>  -->
	  	</div>
	  	<div class="form-group">
	    	<label for="last_name">Last name</label>
	    	<input type="text" class="form-control" id="last_name" name="last_name"  value="{{ $last_name }}">
	    	<button type="submit" class="btn btn-primary">
	    		<a href="#" style="color:white">Update</a>
	    	</button>
	  	</div>
	  	<div class="form-group">
	    	<label for="email">First name</label>
	    	<input type="text" class="form-control" id="first_name" name="first_name" value="{{ $first_name }}">
	    	<button type="submit" class="btn btn-primary">
	    		<a href="#" style="color:white">Update</a>
	    	</button>
	  	</div>
	  	<div class="file-input">
	  		<label>Select file ...</label>
	  		<div class="file-preview">
	  			<!-- <div class="close fileinput-remove">x</div> -->
	  			<div class>
	  				<div class="file-preview-thumbnails">
	  					<!-- add image here-->
	  					<div class="item-image" id="item-image" data-file="{{$photo}}">
	  						<img src="{{URL::asset('uploads/thumbs/100_'.$photo)}}" class="image-file-upload img-thumbnail" data-file="{{$photo}}">
	  						<a href="#" class="image-upload-delete"><i class="glyphicon glyphicon-remove-sign"></i></a>
	  						<input type="hidden" class="file_upload" name="file_upload" value="{{$photo}}" />
	  					</div>
	  				</div>
	  				<div class="progress" id="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
						    <span class="progressText"></span>
						</div>
					</div>
	  			</div>
	  		</div>
	  		<div class="input-group">
			  	<div tabindex="500" class="form-control file-caption  kv-fileinput-caption">
			  		<div class="file-caption-name"></div>
			  	</div>
			  	<div class="input-group-btn" >
			  		<div tabindex="500" class="btn btn-primary btn-file">
			    		<i class="glyphicon glyphicon-folder-open"></i>&nbsp;Browse …
			    		<input id="file_upload" type="file" class="file">  
			   		</div>	
			  	</div>
		  	</div>
	  	</div>
		  		  	
	  	<button type="submit" class="btn btn-primary">Update</button>
	</form>

@stop

@section('script')
	<script src="{{ URL::asset('js/upload.js') }}"></script>
	<script src="{{ URL::asset('js/update-profile.js') }}"></script>
@stop