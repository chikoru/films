@extends('layouts.main')

@section('title')
	<title>Admin Page</title>
@stop
	<style>
		.custom-search-form{
		    margin-top:5px;
		}

		.glyphicon{
			height: 20px;
		}

		.container{
			padding-left: 0px !important;
			position: relative;
		}

		.info{
			float: left;
			position: relative;
			border: 1px solid gray;
			width: 15%;
			margin:5px 5px 0px 5px;
			padding-left: 25px;
		}
		.actions{
			float: left;
			position: relative;
			width: 40%;
			margin: 5px 5px 0px 5px;
		}
		.search {
			float: left;
			position: relative;
			width: 30%;
			margin: 0px 5px 0px 5px;
		}
	
	</style>
@section('css')

@stop

@section('content')

	<h1>ADMIN PAGE</h1>


	@if(Auth::check())	

		<div class="container">
			<div class="info">
				Xin chào <strong> {{ Auth::user()->first_name }} </strong><br/>
				<img src="{{ URL::asset('uploads/thumbs/100_'.Auth::user()->photo) }}" alt="Avatar"><br/><br/>
			</div>
			
			<div class="actions">
				<button type="submit" class="btn btn-primary">
				<a style="color:white" href="{{ URL::route('account-sign-out') }}">Sign Out</a>
				</button>
				<button type="submit" class="btn btn-primary">
					<a style="color:white" href="{{ URL::route('account-view-profile') }}">View Profile</a>
				</button>
				<button type="submit" class="btn btn-primary">
					<a style="color:white" href="{{ URL::route('administrator-create-view') }}">Create</a>
				</button>
				<button type="submit" class="btn btn-primary">
					<a style="color:white" href="{{ URL::route('administrator-users-list') }}">Delete</a>
				</button>
				<button type="submit" class="btn btn-primary">
					<a style="color:white" href="{{ URL::route('account-view-profile') }}">Block</a>
				</button>
			</div>

			<div class="search">
				<div class="row">
					<div class="col-lg-12">
			            <div class="input-group custom-search-form">
			             	<input type="text" class="form-control">
			             	<span class="input-group-btn">
			              		<button class="btn btn-default" type="button">
			              			<span class="glyphicon glyphicon-search"></span>
			             		</button>
			             	</span>
			             </div>
			        </div>
				</div>
			</div>

		</div>

	@else
		<button type="submit" class="btn btn-primary">
			<a style="color:white" href="{{ URL::route('get-login') }}">Login</a>
		</button>	
	@endif
	
	@if(Session::has('global'))
		<div class="alert alert-info">{{ Session::get('global') }}</div>
	@endif
		
		
@stop 

@section('script')

@stop