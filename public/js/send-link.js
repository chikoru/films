(function ($){

  $('form#send-link').on('submit', function(){

  	  var form = $(this),
      		url = form.attr('action'),
      		method = form.attr('method');

    	//regex
    	var email_regex = /^[\w\-\.\_\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

      // Check null
      if (form.find('#email').val() == '') {
          form.find('.alert').showMessageHTML('Lỗi!','Bạn phải nhập đầy đủ thông tin.','danger');
          return false;
      }
      
      // Check email
      if (!form.find('#email').val().match(email_regex)) {
        form.find('#email').parent().addError();
        form.find('.alert').showMessageHTML('Lỗi!','Vui lòng nhập đúng email.','danger');
        return false;
      }

  	  $.ajax({
    	  	type: 'post',
    	  	url: url,
    	  	data: form.serialize(),
    	   	dataType: 'json',
    	  	success: function(res){
            if (res.success == true) {
              form.find('.alert').showMessageHTML('Thành công!','Gửi thành công. Vui lòng kiểm tra mail của bạn.','success');
            } else {
              form.find('.alert').showMessageHTML('Thông báo!','Email không tồn tại.','info');
            }
    	  		 
    	  	},
    	  	error: function(){
            form.find('.alert').showMessageHTML('Cảnh báo!','Có lỗi xảy ra.','warning');
    	  	}
  	  });

      return false;
  });

})(jQuery)