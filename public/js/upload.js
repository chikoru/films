$(document).on('change', '#file_upload', function(){

		var source = this.files;

		var site_url = "http://films.app.com/";

		var formdata = new FormData(), i;

		for (i = 0; i < source.length; i = i + 1) {
			formdata.append('files[]', source[i]);
		};	
		/*$('.file-preview').show();*/

		$.ajax({
			xhr: function() {
				/*$('#product-request').find('.loading-wrapper').show();*/

				$('#progress .progress-bar').css('width', 0 + '%');
				$("#progress").show();
				$('.file-preview').show();
			    var xhr = new window.XMLHttpRequest();
			    xhr.upload.addEventListener("progress", function(evt) {

			      if (evt.lengthComputable) {

			        var percentComplete = evt.loaded / evt.total;
			        percentComplete = parseInt(percentComplete * 100);

			        $('#progress .progress-bar').css('width', percentComplete + '%');

	        		$('#progress .progressText').html(percentComplete + '%');

			        if(percentComplete === 100){
			        	$("#progress").delay(500).fadeOut('normal');
			        }
			      }
			    }, false);

			    return xhr;
			},
		   	url: site_url + "user/upload-file",
		    type: 'post',
		    data: formdata,
		    processData: false,
		    contentType: false,
		    dataType: 'json',
		    success: function (data) {
		    	//if (isset($('#item-image'))) {
		     		$('.item-image').remove();
		     	//}
		     	$.each(data.files, function (index, file) {
	                $('.file-preview-thumbnails').append('<div class="item-image" id="item-image" data-file="'+ file.name +'"><img src="'+ site_url +'uploads/thumbs/'+ file.thumb +'" class="image-file-upload img-thumbnail" data-file="'+ file.name +'"><a href="#" class="image-upload-delete"><i class="glyphicon glyphicon-remove-sign"></i></a><input type="hidden" class="file_upload" name="file_upload" value="'+file.name+'" />');
	            });
	            /*$('input[type="file"]').val('');*/
	           /* $('#product-request').find('.loading-wrapper').hide();*/
		    }
		});
	});

	//Remove file upload
   	$(document).on('click', '.image-upload-delete', function(event){
   		event.preventDefault();

   		var site_url = "http://films.app.com/";
   		/*$('#product-request').find('.loading-wrapper').show();*/
		var self = $(this).parent();
   		var file = self.attr('data-file')	
		$.ajax({
			url: site_url + 'user/delete-file',
			type: 'post',
			dataType: 'json',
			data: { fname: file },
			success: function(data){
				if(data.success === true){
					self.fadeOut('slow', function(){
					    self.remove();
					    $('#product-request').find('.loading-wrapper').hide();
					});
				}
			}
		});		   		
   });
