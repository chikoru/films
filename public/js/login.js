(function($){

	$('form#login').on('submit', function(){

		  var form = $(this),
    			url = form.attr('action'),
    			method = form.attr('method');

		//regex
    	var email_regex = /^[\w\-\.\_\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    	    	
		//flag    
    	var flag = true;	

    	// Check fill out information
    	if (form.find('#email').val() == '') {
    		flag = false;
    	}
    	if (form.find('#password').val() == '') {
    		flag = false;
    	}
    	if (!flag) {
    		form.find('.alert').showMessageHTML('Lỗi!','Bạn phải nhập đầy đủ thông tin.','danger');
    		return false;
    	}

    	// Check email
	    if (!form.find('#email').val().match(email_regex)) {
	        form.find('#email').parent().addError();
	        form.find('.alert').showMessageHTML('Lỗi!','Vui lòng nhập đúng email.','danger');
	        return false;
	    }

	    // Check password 
	    if (form.find('#password').val().length <= 6 || form.find('#password').val().length > 20) {
	        form.find('#password').parent().addError();
	        form.find('.alert').showMessageHTML('Lỗi!','Mật khẩu phải từ 6-20 kí tự.','danger');
	        return false;
	    }

	    $.ajax({
  	    	type : 'post',
  	    	url : url,
  	    	data : form.serialize(),
  	    	dataType : 'json',
  	    	success : function(res){
            if (res.success == true) {
              form.find('.alert').showMessageHTML('Thành công!','Đăng nhập thành công.','success');
            } else {
              form.find('.alert').showMessageHTML('Lỗi!','Sai tài khoản hoặc mật khẩu.','danger');
            }
  	    	},
  	    	error : function(){
            form.find('.alert').showMessageHTML('Lỗi!','Có lỗi xảy ra.','danger');
  	    	} 
	    });

      return false;
	    
	});

  

})(jQuery)