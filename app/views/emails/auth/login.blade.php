@extends('layouts.main')

@section('title')
  <title>Login</title>
@stop

@section('css')
  <link rel="stylesheet" href="{{ URL::asset('css/site.css') }}">
@stop

@section('content')


@if(Session::has('global'))
    {{ Session::get('global') }}
@endif
<form class="form-horizontal" id="login" action="{{ URL::route('post-login') }}" method="post">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="text" name="email" class="form-control" {{ Input::old('email') ? 'value="' . e(Input::old('email')) .'"' : '' }}>
          @if($errors->has('email'))
            {{ $errors->first('email') }}
          @endif
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
      <input type="password" name="password" class="form-control">
          @if($errors->has('password'))
            {{ $errors->first('password') }}
          @endif
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox" id="remember" name="remember"> Remember me
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">Sign in</button>
      <a href="{{ URL::route('account-forgot-password') }}">Forgot password ???</a>
    </div>

  </div>
  {{ Form::token() }}
</form>

@stop

@section('script')
    
@stop