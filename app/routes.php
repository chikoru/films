<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//home
Route::get('/', array('as' => 'home', 'uses' => 'HomeController@showWelcome' ));

Route::group(array('prefix' => 'user'), function()
{
    //register --------OK
	Route::get('/create', array('as' => 'get-create','uses' => 'AccountController@getCreate'));

	Route::post('/create', array('before' => 'csrf', 'as' => 'post-create', 'uses' => 'AccountController@postCreate'));

	// upload and delete -------Not OK
	Route::post('/upload-file', array('as' => 'upload-file','uses' => 'UploadController@upload_file'));

	Route::post('/delete-file', array('as' => 'delete-file','uses' => 'UploadController@delete_file'));

	// active-
	Route::get('/activate-account/{code}', array('as' => 'activate-account', 'uses' => 'AccountController@activate'));

	//login
	Route::get('/login', array('before' => 'checklogin', 'as' => 'get-login', 'uses' => 'AccountController@getLogin'));
	Route::post('/login', array('before'=>'csrf', 'as' => 'post-login', 'uses' => 'AccountController@postLogin'));

	//logout
	Route::get('/signout', array('as' => 'account-sign-out', 'uses' => 'AccountController@signout'));

	// Forgot password
	Route::post('/forgot-password', array('before' => 'csrf', 'as' => 'post-forgot-password', 'uses' => 'AccountController@postForgotPassword'));

	Route::get('/forgot-password', array('before' => 'checklogin', 'as' => 'account-forgot-password', 'uses' => 'AccountController@getSendEmail'));

	Route::get('/account-recover/{code}', array('before' => 'force-logout', 'as' => 'account-recover', 'uses' => 'AccountController@getRecover'));

	// Change password
	Route::get('/change-password', array('before' => 'guest', 'as' => 'get-change-password', 'uses' => 'AccountController@getChangePassword'));
	Route::post('/change-password', array('before' => 'csrf', 'as' => 'post-change-password', 'uses' => 'AccountController@postChangePassword'));

	// View profile
	Route::get('/view-profile', array('as' => 'account-view-profile', 'uses' => 'AccountController@viewProfile'));

	//Update profile
	Route::post('/update-profile', array('as' => 'account-update-profile', 'uses' => 'AccountController@updateProfile'));
});


Route::group(array('prefix' => 'admin'), function()
{

    // Admin
	Route::get('/admin-view', array('before' => 'check-user-level' , 'as' => 'administrator-account-users', 'uses' => 'AdminController@viewAdminPage'));
	// Admin create user
	Route::get('/admin-create', array('as' => 'administrator-create-view', 'uses' => 'AdminController@getCreate'));
	Route::post('/admin-create', array('as' => 'administrator-create-post', 'uses' => 'AdminController@postCreate'));
	// User list
	Route::get('/users-list', array('as' => 'administrator-users-list', 'uses' => 'AdminController@getUsersList'));
	// Delete user
	Route::post('/delete-user/{id}', array('as' => 'administrator-delete-user', 'uses' => 'AdminController@deleteUser'));


});



// Check auth
Route::group(array('before' => 'checklogin'), function()
{
  	Route::get('/user/login', array(
		'as' => 'get-login',
		'uses' => 'AccountController@getLogin'
	));
});


//------------
Route::post('messages/taskqueues/{queue_name}', 'ProducerController@taskQueues');



// ----URL PATTERNS
Route::pattern('id', '[0-9]+');
Route::pattern('code', '[0-9a-zA-Z]+');