@extends('layouts.main')

@section('title')
	<title>Change Password</title>
@stop

@section('css')
  <link rel="stylesheet" href="{{ URL::asset('css/site.css') }}">
  <style type="text/css">
		#email{
			width : 80%;
			display: inline;
		}
  </style>
@stop

@section('content')
	
	@if(isset($global))
		{{ $global }}
	@endif

	<form class="form-horizontal" id="send-link" action="{{ URL::route('post-forgot-password') }}" method="post">
		<span class="alert"></span>
	  	<div class="form-group">
	    <label for="tmess" class="col-sm-6 control-label">Vui lòng nhập email của bạn để thay đổi password.</label>
	    <div class="col-sm-10">
	      	<input type="text" name="email" id ="email" class="form-control" >

		    <button type="submit" class="btn btn-primary">Send</button> 
	    </div>
	  	</div>
	 	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
	</form>

@stop

@section('script')
	<script src="{{ URL::asset('js/send-link.js') }}"></script>
@stop